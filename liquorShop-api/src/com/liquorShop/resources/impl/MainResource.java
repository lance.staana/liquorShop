package com.liquorShop.resources.impl;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
public class MainResource{

	@GET
	@Produces(MediaType.TEXT_HTML)
	public Response index() {
	
		return Response.ok("<html> " + "<title>" + "LiquorShop" + "</title>"
            + "<body><h1>" + "Welcome to liquorShop API" + "</h1></body>" + "</html> ").build();    
	}

	public Response get() {
		// TODO Auto-generated method stub
		return null;
	}

	public Response add() {
		// TODO Auto-generated method stub
		return null;
	}

	public Response delete() {
		// TODO Auto-generated method stub
		return null;
	}

	public Response update() {
		// TODO Auto-generated method stub
		return null;
	}

}
