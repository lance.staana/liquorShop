package com.liquorShop.resources.impl;

import static javax.ws.rs.core.Response.created;
import static javax.ws.rs.core.Response.noContent;
import static javax.ws.rs.core.Response.notModified;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import com.liquorShop.models.representation.LiquorDetails;
import com.liquorShop.models.representation.LiquorDetailsShort;
import com.liquorShop.models.representation.LiquorInput;
import com.liquorShop.resources.ILiquorResource;
import com.liquorShop.services.ILiquorService;
import com.liquorShop.services.impl.LiquorServiceImpl;

@Path("/liquors")
public class LiquorResourceImpl implements ILiquorResource<LiquorInput, LiquorDetails>{
	
	@Context UriInfo uriInfo;
	ILiquorService liquorService = new LiquorServiceImpl();
	
	private final static Status LIQUOR_NOT_FOUND = Response.Status.NOT_FOUND;
	private final static Status LIQUOR_DELETED = Response.Status.ACCEPTED;
	
	@Override
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response index() {
		
		Response response = noContent().build();
		List<LiquorDetailsShort> liquorList = liquorService.getAll();
				
		if (!liquorList.isEmpty()) {
			response = ok(liquorList).build();
		}
		
		return response;
	}
	
	@Override
	@GET
	@Path("/{liquorId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("liquorId") String liquorId) {
		
		Response response = status(LIQUOR_NOT_FOUND).build();
		LiquorDetails liquorDetails = liquorService.get(Integer.parseInt(liquorId));
		
		if (liquorDetails != null) {
			response = ok(liquorDetails).build();
		}
		
		return response;
	}

	@Override
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response add(LiquorInput newLiquorEntry) {
		
		Response response = noContent().build();
		LiquorDetails savedLiquor = liquorService.insert(newLiquorEntry);
		
		if (savedLiquor != null && savedLiquor.getId() != null) {
			
			String savedLiquorId = savedLiquor.getId();
			UriBuilder builder = uriInfo.getAbsolutePathBuilder().path(savedLiquorId);
			response = created(builder.build()).build();
		}
		
		return response;
	}

	@Override
	@DELETE
	@Path("/{liquorId}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response delete(@PathParam("liquorId") String liquorId) {
		
		Response response = notModified().build();
		
		Boolean isLiquorDeleted = liquorService.delete(Integer.parseInt(liquorId));
		
		if (isLiquorDeleted) {
			response = status(LIQUOR_DELETED).build();
		}
		
		return response;
	}

	@Override
	@PUT
	@Path("/{liquorId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("liquorId") String liquorId,
			LiquorDetails updatedLiquorDetails) {
		
		Response response = notModified().build();
		
		if (liquorId != null && updatedLiquorDetails.getId() != null) {

			if (liquorId.equals(updatedLiquorDetails.getId())) {
				
				Boolean isLiquorUpdated = liquorService.update(updatedLiquorDetails);
				
				if (isLiquorUpdated) {
					response = ok().build();
				}
			}
		}
		
		return response;
	}

	
	static LiquorResourceImpl liquorServiceSi = new LiquorResourceImpl();
	
	/**
	 * Extracted method for testing purposes
	 * @return
	 */
	String iamMarriedCaller() {
		
		return "May asawa na ako";
	}
	
	public static String iamMarried() {
		
		return liquorServiceSi.iamMarriedCaller();
	}

	
	
	
	
	/**
		TODO
		> testing private
			- caller
			- pk protected
		> testing void
		> testing static
			- method extract
		> test suite
		> parameterized tests
	*/
}
