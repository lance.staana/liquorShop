package com.liquorShop.resources;

import javax.ws.rs.core.Response;

import com.liquorShop.models.representation.LiquorDetails;
import com.liquorShop.models.representation.LiquorInput;

/**
* @author lance.sta.ana@uap.asia
*/

public interface ILiquorResource<I, D> {

	public Response index();
	public Response get(String liquorId);
	public Response add(I newLiquorEntry);
	public Response delete(String liquorId);
	public Response update(String liquorId, D updatedLiquorDetails);
}
