package com.liquorShop.dao;

import java.util.List;

/**
 * 
 * @author lance.sta.ana@uap.asia
 */

public interface IEntityDao<E> {

	public List<E> getAll();
	public E getById(E reqEntity);
	public E save(E newEntity);
	public Boolean update(E updatedEntity);
	public Boolean delete(E entityData);
	public List<E> find(E reqEntity);
}
