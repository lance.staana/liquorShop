package com.liquorShop.dao.impl;

import java.util.List;

import com.liquorShop.dao.IEntityDao;
import com.liquorShop.data.TestData;
import com.liquorShop.models.entities.Liquor;

public class LiquorDaoImpl implements IEntityDao<Liquor>{

	@Override
	public List<Liquor> getAll() {

		List<Liquor> liquors = TestData.getLiquors();
		
		System.out.println("TestData.getAll was used");
		return liquors;
	}

	@Override
	public Liquor getById(Liquor reqEntity) {
		
		Liquor reqLiquor = null;
		
		if (reqEntity != null 
				&& reqEntity.getId() != null) {
			
			Integer liquorId = reqEntity.getId();
			if (TestData.getLiquors().get(liquorId) != null) {

				reqLiquor = TestData.getLiquors().get(liquorId);
			}
		}

		System.out.println("TestData.getById was used");
		
		return reqLiquor;
	}

	@Override
	public Liquor save(Liquor newEntity) {
		
		Liquor savedLiquor = null;
		
		if (newEntity != null) {
			
			Integer newLiquorId = TestData.getLiquors().size();
			newEntity.setId(newLiquorId);
			
			List<Liquor> liqList = TestData.getLiquors();
			liqList.add(newEntity);
			
			savedLiquor = TestData.getLiquors().get(newLiquorId);
		}
		
		System.out.println("TestData.save was used");
		return savedLiquor;
	}

	@Override
	public Boolean update(Liquor updatedEntity) {
		
		Boolean isLiquorUpdated = false;
		
		//updated entity must not be null
		if (updatedEntity != null && updatedEntity.getId() != null) {
			
			Liquor entityToUpdate = this.getById(updatedEntity);
			
			if (entityToUpdate != null) {
				
				entityToUpdate.setName(updatedEntity.getName());
				entityToUpdate.setOrigin(updatedEntity.getOrigin());
				entityToUpdate.setProof(updatedEntity.getProof());
				entityToUpdate.setSellingPrice(updatedEntity.getSellingPrice());
				entityToUpdate.setType(updatedEntity.getType());
			
				TestData.getLiquors().set(entityToUpdate.getId(), entityToUpdate);
				isLiquorUpdated = true;
			}
		}
		
		System.out.println("TestData.update was used");
		return isLiquorUpdated;
	}

	@Override
	public Boolean delete(Liquor entityData) {
		
		Boolean isLiquorDeleted = false;
		
		if (entityData != null && entityData.getId() != null) {
			
			Liquor entityToDelete = this.getById(entityData);
			
			if (entityToDelete != null) {
				
				isLiquorDeleted = TestData.getLiquors().remove(entityToDelete);
			}
		}
		
		System.out.println("TestData.delete was used");
		return isLiquorDeleted;
	}

	@Override
	public List<Liquor> find(Liquor reqEntity) {
		// TODO Auto-generated method stub
		return null;
	}

}
