package com.liquorShop.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.liquorShop.models.entities.Liquor;

public class TestData {
	
	private static List<Liquor> liquors = new ArrayList<Liquor>(Arrays.asList(
			
		new Liquor() {
			{
				setId(0);
				setName("Grey Goose");
				setOrigin("France");
				setProof(70.00);
				setSellingPrice(1990.00);
				setType(Liquor.LIQUOR_TYPE_VODKA);
			}
		},
		
		new Liquor() {
			{
				setId(1);
				setName("Gordons");
				setOrigin("London");
				setProof(70.00);
				setSellingPrice(1700.00);
				setType(Liquor.LIQUOR_TYPE_GIN);
			}
		},
		
		new Liquor() {
			{
				setId(2);
				setName("McAllan");
				setOrigin("Scotland");
				setProof(60.00);
				setSellingPrice(2400.00);
				setType(Liquor.LIQUOR_TYPE_WHISKEY);
			}
		},
		
		new Liquor() {
			{
				setId(3);
				setName("Don Julio");
				setOrigin("Mexico");
				setProof(80.00);
				setSellingPrice(2100.00);
				setType(Liquor.LIQUOR_TYPE_TEQUILA);
			}
		},
		
		new Liquor() {
			{
				setId(4);
				setName("Tiger Black");
				setOrigin("Singapore");
				setProof(15.00);
				setSellingPrice(49.00);
				setType(Liquor.LIQUOR_TYPE_BEER);
			}
		}
	));
	
	public static List<Liquor> getLiquors(){
		return liquors;
	}
}
