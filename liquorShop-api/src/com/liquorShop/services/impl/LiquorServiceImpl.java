package com.liquorShop.services.impl;

import java.util.ArrayList;
import java.util.List;

import com.liquorShop.dao.IEntityDao;
import com.liquorShop.dao.impl.LiquorDaoImpl;
import com.liquorShop.models.entities.Liquor;
import com.liquorShop.models.representation.LiquorDetails;
import com.liquorShop.models.representation.LiquorDetailsShort;
import com.liquorShop.models.representation.LiquorInput;
import com.liquorShop.services.ILiquorService;

public class LiquorServiceImpl implements ILiquorService{

	IEntityDao<Liquor> liquorDao = new LiquorDaoImpl();
	private static Double VAT_PCT = 0.12; 
	
	String buildUrl(Integer liquorId) {
		
		//null checks
		String liquorIdString = liquorId.toString();
		String localUrl = "http://localhost:8080/liquorShop-api/liquors/";
		
		return localUrl + liquorIdString;
	}
	
	private static Double buildVatAmount(Double sellingPrice) {
		
		//null checks
		Double vatAmount = sellingPrice * VAT_PCT;
		return vatAmount;
	}

	private String buildTotalPrice(Double sellingPrice) {
		
		return buildTotalPriceCaller(sellingPrice);
	}

	String buildTotalPriceCaller(Double sellingPrice) {
		Double vatAmount = buildVatAmount(sellingPrice);
		Double totalSellingPrice = sellingPrice + vatAmount;
		
		String totalSellingPriceString = String.format("%.2f", totalSellingPrice);
		
		return totalSellingPriceString;
	}
	
	@Override
	public List<LiquorDetailsShort> getAll() {
		
		List<LiquorDetailsShort> liquorList = new ArrayList<LiquorDetailsShort>();
		List<Liquor> liquorListData = liquorDao.getAll();
		
		if (liquorListData != null) {
			
			for (Liquor liquor : liquorListData) {
				
				if (liquor != null) {
					
					LiquorDetailsShort liquorDetailsShort = new LiquorDetailsShort();
					liquorDetailsShort.setTotalPrice(buildTotalPrice(liquor.getSellingPrice()));
					liquorDetailsShort.setName(liquor.getName());
					liquorDetailsShort.setType(liquor.getType());
					liquorDetailsShort.setUrl(buildUrl(liquor.getId()));
					liquorList.add(liquorDetailsShort);
				}
			}
		}
		
		return liquorList;
	}

	@Override
	public LiquorDetails get(Integer reqLiquorid) {
		
		LiquorDetails liquorDetail = null;
		
		if (reqLiquorid != null) {
		
			Liquor reqLiquor = new Liquor();
			reqLiquor.setId(reqLiquorid);
			
			Liquor liquorData = liquorDao.getById(reqLiquor);
			
			if (liquorData != null) {
				
				liquorDetail = new LiquorDetails();
				liquorDetail.setId(liquorData.getId().toString());
				liquorDetail.setName(liquorData.getName());
				liquorDetail.setOrigin(liquorData.getOrigin());
				liquorDetail.setProof(liquorData.getProof().toString());
				liquorDetail.setSellingPrice(liquorData.getSellingPrice().toString());
				liquorDetail.setTotalPrice(buildTotalPrice(liquorData.getSellingPrice()));
				liquorDetail.setType(liquorData.getType());
				liquorDetail.setVat(buildVatAmount(liquorData.getSellingPrice()).toString());
			}
		}
		
		logger();
		return liquorDetail;
			
	}

	@Override
	public LiquorDetails insert(LiquorInput newLiquor) {
		
		LiquorDetails savedLiquor = null;
		
		Liquor newLiquorData = new Liquor();
		newLiquorData.setName(newLiquor.getName());
		newLiquorData.setOrigin(newLiquor.getOrigin());
		newLiquorData.setProof(Double.parseDouble(newLiquor.getProof()));
		newLiquorData.setSellingPrice(Double.parseDouble(newLiquor.getSellingPrice()));
		newLiquorData.setType(newLiquor.getType());
		
		newLiquorData = liquorDao.save(newLiquorData);
		
		if (newLiquorData != null 
				&& newLiquorData.getId() != null) {
			
			savedLiquor = this.get(newLiquorData.getId());
		}
		
		return savedLiquor;
	}

	@Override
	public boolean delete(Integer liquorId) {
	
		boolean isLiquorDeleted = false;
		
		if (liquorId != null) {
			
			Liquor liquorToDelete = new Liquor();
			liquorToDelete.setId(liquorId);
			
			isLiquorDeleted = liquorDao.delete(liquorToDelete);
		}
		
		return isLiquorDeleted;
	}

	@Override
	public boolean update(LiquorDetails toUpdate) {
	
		boolean isLiquorUpdated = false;
		
		if (toUpdate != null && toUpdate.getId() !=null) {
			
			Liquor reqEntity = new Liquor();
			reqEntity.setId(Integer.parseInt(toUpdate.getId()));
			
			Liquor toBeUpdated = liquorDao.getById(reqEntity);
			
			if (toBeUpdated != null) {
				
				toBeUpdated.setName(toUpdate.getName());
				toBeUpdated.setOrigin(toUpdate.getOrigin());
				toBeUpdated.setProof(Double.parseDouble(toUpdate.getProof()));
				toBeUpdated.setSellingPrice(Double.parseDouble(toUpdate.getSellingPrice()));
				toBeUpdated.setType(toUpdate.getType());
				
				isLiquorUpdated = liquorDao.update(toBeUpdated);
			}
		}
		
		return isLiquorUpdated;
	}

	
	
	
	void logger() {
		System.out.println("asdsadsadas");
	}
	
}
