package com.liquorShop.services;

import java.util.List;

import com.liquorShop.models.entities.Liquor;
import com.liquorShop.models.representation.LiquorDetails;
import com.liquorShop.models.representation.LiquorDetailsShort;
import com.liquorShop.models.representation.LiquorInput;

public interface ILiquorService {

	
	public List<LiquorDetailsShort> getAll();
	public LiquorDetails get(Integer reqLiquorId);
	public LiquorDetails insert(LiquorInput newLiquor);
	public boolean delete(Integer liquorId);
	public boolean update(LiquorDetails toUpdate);
}
