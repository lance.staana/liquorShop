package com.liquorShop.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.liquorShop.dao.impl.LiquorDaoImplTest;
import com.liquorShop.resources.impl.LiquorResourceImplTest;
import com.liquorShop.services.impl.LiquorServiceImplTest;

@RunWith(Suite.class)
@SuiteClasses({

	LiquorDaoImplTest.class,
	LiquorServiceImplTest.class,
	LiquorResourceImplTest.class
})

public class AllTests {}
