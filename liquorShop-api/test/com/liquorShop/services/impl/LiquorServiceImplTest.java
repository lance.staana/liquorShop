package com.liquorShop.services.impl;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.liquorShop.dao.IEntityDao;
import com.liquorShop.dao.impl.LiquorDaoImpl;
import com.liquorShop.models.entities.Liquor;
import com.liquorShop.models.representation.LiquorDetails;
import com.liquorShop.models.representation.LiquorDetailsShort;
import com.liquorShop.models.representation.LiquorInput;

@RunWith(MockitoJUnitRunner.class)
public class LiquorServiceImplTest {

	@Mock
	IEntityDao<Liquor> mockedDao;
	
	@Spy
	@InjectMocks
	LiquorServiceImpl sut;
	
	@Before 
	public void setup() {
		
		List<Liquor> liquorList = new ArrayList<Liquor>();
		liquorList.add(mock(Liquor.class));
		doReturn(liquorList).when(mockedDao).getAll();
	}
	
	@Test
	public void getAllTest() {
	
		List<LiquorDetailsShort> liquors = null;
		liquors = sut.getAll();
		
		assertNotNull(liquors);
		assertFalse(liquors.isEmpty());
		assertNotNull(liquors.get(0));
		assertEquals(LiquorDetailsShort.class, liquors.get(0).getClass());
	}
	
	@Test
	public void getTest() {
		
		Liquor liquorData = new Liquor();
		liquorData.setId(0);
		liquorData.setName(RandomStringUtils.randomAlphabetic(12));
		liquorData.setOrigin(RandomStringUtils.randomAlphabetic(12));
		liquorData.setProof(RandomUtils.nextDouble());
		liquorData.setSellingPrice(RandomUtils.nextDouble());
		liquorData.setType(Liquor.LIQUOR_TYPE_VODKA);
		doReturn(liquorData).when(mockedDao).getById(any(Liquor.class));
		
		LiquorDetails liquorDetail = null;
		liquorDetail = sut.get(1231);
		
		assertNotNull(liquorDetail);
		assertNotNull(liquorDetail.getId());
		assertNotNull(liquorDetail.getName());
		assertNotNull(liquorDetail.getOrigin());
		assertNotNull(liquorDetail.getProof());
		assertNotNull(liquorDetail.getSellingPrice());
		assertNotNull(liquorDetail.getTotalPrice());
		assertNotNull(liquorDetail.getType());
		assertNotNull(liquorDetail.getVat());
	}
	
	@Test
	public void insertTest() {
		
		String name = RandomStringUtils.randomAlphabetic(12);
		String origin = RandomStringUtils.randomAlphabetic(12);
		Double proof = RandomUtils.nextDouble();
		Double sellingPrice = RandomUtils.nextDouble();
		String type = Liquor.LIQUOR_TYPE_VODKA;
		
		LiquorInput newLiquor = new LiquorInput();
		newLiquor.setName(name);
		newLiquor.setOrigin(origin);
		newLiquor.setProof(proof.toString());
		newLiquor.setSellingPrice(sellingPrice.toString());
		newLiquor.setType(type);
		
		Liquor liquorData = new Liquor();
		liquorData.setId(0);
		liquorData.setName(name);
		liquorData.setOrigin(origin);
		liquorData.setProof(proof);
		liquorData.setSellingPrice(sellingPrice);
		liquorData.setType(Liquor.LIQUOR_TYPE_VODKA);
		doReturn(liquorData).when(mockedDao).save(any(Liquor.class));
		doReturn(liquorData).when(mockedDao).getById(any(Liquor.class));
		
		LiquorDetails savedLiquor = null;
		savedLiquor = sut.insert(newLiquor);
		
		assertNotNull(savedLiquor);
		assertNotNull(savedLiquor.getId());
		assertNotNull(savedLiquor.getName());
		assertNotNull(savedLiquor.getOrigin());
		assertNotNull(savedLiquor.getProof());
		assertNotNull(savedLiquor.getSellingPrice());
		assertNotNull(savedLiquor.getTotalPrice());
		assertNotNull(savedLiquor.getType());
		assertNotNull(savedLiquor.getVat());
	
		assertEquals(newLiquor.getName(), savedLiquor.getName());
		assertEquals(newLiquor.getOrigin(), savedLiquor.getOrigin());
		assertEquals(newLiquor.getProof(), savedLiquor.getProof());
		assertEquals(newLiquor.getSellingPrice(), savedLiquor.getSellingPrice());
		assertEquals(newLiquor.getType(), savedLiquor.getType());
	}
	
	@Test
	public void deleteTest() {
		
		Integer liquorId = RandomUtils.nextInt();
		doReturn(true).when(mockedDao).delete(any(Liquor.class));
		
		boolean isLiquorDeleted = false;
		isLiquorDeleted = sut.delete(liquorId);
		
		assertTrue(isLiquorDeleted);
	}
	
	@Test
	public void updateTest() {

		Integer id = RandomUtils.nextInt();
		String name = RandomStringUtils.randomAlphabetic(12);
		String origin = RandomStringUtils.randomAlphabetic(12);
		Double proof = RandomUtils.nextDouble();
		Double sellingPrice = RandomUtils.nextDouble();
		String type = Liquor.LIQUOR_TYPE_VODKA;
		
		LiquorDetails toUpdate = new LiquorDetails();
		toUpdate.setId(id.toString());
		toUpdate.setName(name);
		toUpdate.setOrigin(origin);
		toUpdate.setProof(proof.toString());
		toUpdate.setSellingPrice(sellingPrice.toString());
		toUpdate.setType(type);
		
		doReturn(mock(Liquor.class)).when(mockedDao).getById(any(Liquor.class));
		doReturn(true).when(mockedDao).update(any(Liquor.class));
		
		boolean isLiquorUpdated = false;
		isLiquorUpdated = sut.update(toUpdate);
		
		assertTrue(isLiquorUpdated);
	}
	
	
}
