package com.liquorShop.resources.impl;

import static javax.ws.rs.core.Response.noContent;
import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.liquorShop.models.representation.LiquorDetails;
import com.liquorShop.models.representation.LiquorDetailsShort;
import com.liquorShop.models.representation.LiquorInput;
import com.liquorShop.services.ILiquorService;

public class LiquorResourceImplTest {

	@Mock
	UriInfo mockedUriInfo;
	
	@Mock
	ILiquorService mockedLiquorService;
	
	@Spy
	@InjectMocks
	LiquorResourceImpl sut;

	{MockitoAnnotations.initMocks(this);}
	
	@Test
	public void indexTest() {
		
		//ARRANGE
		int expectedResponseResult = Response.Status.OK.getStatusCode();
		
		List<LiquorDetailsShort> liquorList = spy(new ArrayList<LiquorDetailsShort>());
		liquorList.add(mock(LiquorDetailsShort.class));
		doReturn(liquorList).when(mockedLiquorService).getAll();
		
		//ACT
		Response response = null;
		response = sut.index();
		
		//ASSERT
		verify(mockedLiquorService, atLeastOnce()).getAll();
		assertNotNull(response);
		assertEquals(expectedResponseResult, response.getStatus());
	}
	
	
	@Test
	public void indexTestWhenLiquorListisEmpty() {
		
		//ARRANGE
		int expectedResponseResult = Response.Status.NO_CONTENT.getStatusCode();
		
		//ACT
		Response response = null;
		response = sut.index();
		
		//ASSERT
		assertNotNull(response);
		assertEquals(expectedResponseResult, response.getStatus());
	}
	
	
	
	@Test
	public void addTest() {
		
		LiquorDetails liquorDetails = new LiquorDetails();
		liquorDetails.setId(RandomStringUtils.randomNumeric(12));
		doReturn(liquorDetails).when(mockedLiquorService).insert(any(LiquorInput.class));
		
		UriBuilder mockedUriBuilder = mock(UriBuilder.class);
		doReturn(mockedUriBuilder).when(mockedUriInfo).getAbsolutePathBuilder();
		doReturn(mock(UriBuilder.class)).when(mockedUriBuilder).path(anyString());
		
		Response response = null;
		response = sut.add(mock(LiquorInput.class));
		
		assertNotNull(response);
	}
	
	
	@Test
	public void iamMarriedTest() {
		
		String actual = "kabit ako";
		doReturn("kabit ako").when(sut).iamMarriedCaller();
		
		String test = LiquorResourceImpl.iamMarried();
		assertNotNull(test);
		assertEquals(test, actual);
		
	}
	
}
