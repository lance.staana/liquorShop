package com.liquorShop.dao.impl;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.liquorShop.data.TestData;
import com.liquorShop.models.entities.Liquor;

//@RunWith(MockitoJUnitRunner.class)
public class LiquorDaoImplTest {

	
	@Test
	public void getByIdTest() {
		
		
		
		
		//ARRANGE
		LiquorDaoImpl sut = new LiquorDaoImpl();
		Liquor reqEntity = new Liquor();
		reqEntity.setId(2);
		
		Double expectedProof = 60.00;
		
		//ACT
		Liquor liquor = null;
		liquor = sut.getById(reqEntity);
		
		
		//ASSERT
		assertNotNull(liquor);
		assertNotNull(liquor.getId());
		assertNotNull(liquor.getName());
		assertNotNull(liquor.getOrigin());
		assertNotNull(liquor.getProof());
		assertNotNull(liquor.getSellingPrice());
		assertNotNull(liquor.getType());
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	@Spy LiquorDaoImpl sut;
//	
//	@Test
//	public void getAllTest() {
//		
//		List<Liquor> liquors = null;
//		liquors = sut.getAll();
//		
//		assertNotNull(liquors);
//		assertFalse(liquors.isEmpty());
//		assertNotNull(liquors.get(0));
//	}
//
//	@Test
//	public void getByIdTest() {
//		
//		Liquor reqLiquor = new Liquor();
//		reqLiquor.setId(0);
//		
//		Liquor liquor = null;
//		liquor = sut.getById(reqLiquor);
//		
//		assertNotNull(liquor);
//		assertNotNull(liquor.getId());
//		assertNotNull(liquor.getName());
//		assertNotNull(liquor.getOrigin());
//		assertNotNull(liquor.getProof());
//		assertNotNull(liquor.getSellingPrice());
//		assertNotNull(liquor.getType());
//	}
//	
//	@Test
//	public void saveTest() {
//
//		Liquor newLiquor = new Liquor();
//		newLiquor.setName(RandomStringUtils.randomAlphabetic(12));
//		newLiquor.setOrigin(RandomStringUtils.randomAlphabetic(12));
//		newLiquor.setProof(RandomUtils.nextDouble());
//		newLiquor.setSellingPrice(RandomUtils.nextDouble());
//		newLiquor.setType(RandomStringUtils.randomAlphabetic(10));
//		
//		Liquor savedLiquor = null;
//		savedLiquor = sut.save(newLiquor);
//		
//		assertNotNull(savedLiquor);
//		assertNotNull(savedLiquor.getId());
//		assertNotNull(savedLiquor.getName());
//		assertNotNull(savedLiquor.getOrigin());
//		assertNotNull(savedLiquor.getProof());
//		assertNotNull(savedLiquor.getSellingPrice());
//		assertNotNull(savedLiquor.getType());
//	}
//	
//	
//	@Test
//	public void updateTest() {
//
//		Liquor reqLiquor = new Liquor();
//		reqLiquor.setId(0);
//		
//		String newLiquorName = RandomStringUtils.randomAlphabetic(12);
//		Double newSellingPrice = RandomUtils.nextDouble();
//		
//		Liquor updatedEntity = sut.getById(reqLiquor);
//		updatedEntity.setName(newLiquorName);
//		updatedEntity.setSellingPrice(newSellingPrice);
//		
//		Boolean isLiquorUpdated = null;
//		isLiquorUpdated = sut.update(updatedEntity);
//	
//		assertNotNull(isLiquorUpdated);
//		assertTrue(isLiquorUpdated);
//
//		Liquor updatedLiquor = sut.getById(reqLiquor);
//		assertNotNull(updatedLiquor);
//		assertEquals(newLiquorName, updatedLiquor.getName());
//		assertEquals(newSellingPrice, updatedLiquor.getSellingPrice());
//	}
//	
//	@Test
//	public void deleteTest() {
//		
//		Integer liquorsBefore = TestData.getLiquors().size();
//		
//		Liquor reqLiquor = new Liquor();
//		reqLiquor.setId(0);
//		
//		Boolean isLiquorDeleted = null;
//		isLiquorDeleted = sut.delete(reqLiquor);
//	
//		Integer liquorsExpectedAfter = liquorsBefore - 1;
//		Integer liquorsExpected = TestData.getLiquors().size();
//	
//		assertNotNull(isLiquorDeleted);
//		assertTrue(isLiquorDeleted);
//		assertEquals(liquorsExpectedAfter, liquorsExpected);
//	}
	
	
	
}
