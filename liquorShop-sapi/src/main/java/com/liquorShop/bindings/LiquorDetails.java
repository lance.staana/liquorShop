package com.liquorShop.bindings;

public class LiquorDetails {

	private String name;
	private String sellingPrice;
	private String proof;
	private String type;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSellingPrice() {
		return sellingPrice;
	}
	
	public void setSellingPrice(String sellingPrice) {
		this.sellingPrice = sellingPrice;
	}
	
	public String getProof() {
		return proof;
	}
	
	public void setProof(String proof) {
		this.proof = proof;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
}
