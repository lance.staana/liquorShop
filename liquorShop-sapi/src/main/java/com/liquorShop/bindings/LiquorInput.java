package com.liquorShop.bindings;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;

public class LiquorInput {

	@NotBlank
	private String name;
	
	@NotBlank
	private String origin;
	
	@NotBlank
	private String type;
	
	@NotBlank
	@DecimalMin(message="must be a decimal", value="10.00")
	private String sellingPrice;
	
	@NotBlank
	@DecimalMin(message="must be a decimal", value="10.00")
	private String proof;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSellingPrice() {
		return sellingPrice;
	}
	public void setSellingPrice(String sellingPrice) {
		this.sellingPrice = sellingPrice;
	}
	public String getProof() {
		return proof;
	}
	public void setProof(String proof) {
		this.proof = proof;
	}
	
}
