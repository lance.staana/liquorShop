package com.liquorShop.services;

import java.util.Set;

import org.springframework.stereotype.Component;

import com.liquorShop.models.Liquor;

@Component
public interface LiquorService {

	Set<Liquor> retrieveAll();
	Long recordLiquor(Liquor newLiquor);
	Liquor retrieveOne(Long requestedLiquorId);
	void deleteLiquorById(Long liquorId);
}
