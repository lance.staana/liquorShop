package com.liquorShop.services.impl;

import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.liquorShop.models.Liquor;
import com.liquorShop.repositories.LiquorRepository;
import com.liquorShop.services.LiquorService;

@Service
public class LiquorServiceImpl implements LiquorService{

	@Autowired
	LiquorRepository liquorRepository;
	
	@Override
	public Set<Liquor> retrieveAll() {
		
		Set<Liquor> liquorData = new HashSet<Liquor>();
		liquorData = liquorRepository.findAll();
		return liquorData;
	}

	@Override
	public Long recordLiquor(Liquor newLiquor) {
	
		Long newLiqourDataId = null;
		Liquor newLiquorData = liquorRepository.save(newLiquor);
		
		if (newLiquorData != null
				&& newLiquorData.getId() != null) {
			
			newLiqourDataId = newLiquorData.getId();
		}
		
		return newLiqourDataId;
	}

	@Override
	public Liquor retrieveOne(Long requestedLiquorId) {
	
		Liquor requestedLiquor = null;
	
		try {
			Optional<Liquor> liqourData = liquorRepository.findById(requestedLiquorId);
			if (liqourData != null) {
				requestedLiquor = liqourData.get();
			}
		} 
		catch (NoSuchElementException e) {
			requestedLiquor = null;
		}
		
		return requestedLiquor;
	}

	@Override
	public void deleteLiquorById(Long liquorId) {
		liquorRepository.deleteById(liquorId);
	}

	
}
