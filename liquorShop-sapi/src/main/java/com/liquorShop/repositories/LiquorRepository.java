package com.liquorShop.repositories;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.liquorShop.models.Liquor;

public interface LiquorRepository extends CrudRepository<Liquor, Long>{

	Set<Liquor> findAll();
}
