package com.liquorShop.controllers;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.liquorShop.bindings.LiquorDetails;
import com.liquorShop.bindings.LiquorElement;
import com.liquorShop.bindings.LiquorInput;
import com.liquorShop.models.Liquor;
import com.liquorShop.services.LiquorService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value="/liquors")
@Api(value="online liquor store", 
description="Operations pertaining to liquors in online Store")
public class LiquorController {

	@Autowired
	LiquorService liquorService;

	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "View a list of available liquors",response = LiquorElement.class)
	public Set<LiquorElement> getAll(UriComponentsBuilder builder) {
		
		Set<LiquorElement> liquorList = new HashSet<LiquorElement>();
		
		Set<Liquor> liquorData = liquorService.retrieveAll();
		if (liquorData != null) {
		
			builder = builder.path("liquors/{id}");
			
			for (Liquor liquor : liquorData) {
				
				LiquorElement liquorElement = new LiquorElement();
				liquorElement.setUri(builder.buildAndExpand(liquor.getId()).toUriString());
				liquorElement.setName(liquor.getName());
				liquorElement.setSellingPrice(liquor.getSellingPrice().toString());
				
				liquorList.add(liquorElement);
			}
		}
		
		return liquorList;
	}
	
	@ApiOperation(value = "Search a liquor with an ID", response = LiquorDetails.class)
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Successfully retrieved a resource"),
		@ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
	@RequestMapping(value = "/{liquorId}", method= RequestMethod.GET)
	public ResponseEntity<?> getOne(@Valid @PathVariable Long liquorId){
		
		ResponseEntity<?> response = ResponseEntity.notFound().build();
		LiquorDetails requestedLiquorDetails = null;
		
		Liquor liquorData = liquorService.retrieveOne(liquorId);
		if (liquorData != null) {
			
			requestedLiquorDetails = new LiquorDetails();
			requestedLiquorDetails.setName(liquorData.getName());
			requestedLiquorDetails.setProof(liquorData.getProof().toString());
			requestedLiquorDetails.setSellingPrice(liquorData.getSellingPrice().toString());
			requestedLiquorDetails.setType(liquorData.getType());
			
			response = ResponseEntity.ok(requestedLiquorDetails);
		}
		
		return response;
	}
	
	@ApiOperation(value = "Add a liquor")    
	@RequestMapping(method=RequestMethod.POST)
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Successfully created a resource"),
			@ApiResponse(code = 401, message = "Bad Request")
	    })
	public ResponseEntity<?> addLiquor(@Valid @RequestBody LiquorInput liquorInput,
			UriComponentsBuilder builder){
		
		ResponseEntity<?> response = ResponseEntity.noContent().build();
		
		Liquor newLiquor = new Liquor();
		newLiquor.setName(liquorInput.getName());
		newLiquor.setOrigin(liquorInput.getOrigin());
		newLiquor.setType(liquorInput.getType());
		newLiquor.setProof(Double.valueOf(liquorInput.getProof()));
		newLiquor.setSellingPrice(Double.valueOf(liquorInput.getSellingPrice()));
		
		Long newLiquorDataId = liquorService.recordLiquor(newLiquor);
		
		if (newLiquorDataId != null) {

			UriComponents uri = builder.path("liquors/{id}").buildAndExpand(newLiquorDataId);
			response = ResponseEntity.created(uri.toUri()).build();
		}
		
		return response;
	}
	
	@ApiOperation(value = "Delete a liquor")
	@RequestMapping(value = "/{liquorId}", method= RequestMethod.DELETE)
	public ResponseEntity<?> deleteLiquor(@Valid @PathVariable Long liquorId){
		
		ResponseEntity<?> response = new ResponseEntity<String>("Nothing was modified", HttpStatus.NOT_MODIFIED);
		liquorService.deleteLiquorById(liquorId);

		Liquor deletedLiquor = null;
		deletedLiquor = liquorService.retrieveOne(liquorId);
		
		if (deletedLiquor == null) {
			response = new ResponseEntity<String>("Liquor was deleted successfully", HttpStatus.ACCEPTED);
		}
		
		return response;
	}
	
	//TODO: https://dzone.com/articles/spring-boot-restful-api-documentation-with-swagger
	//TODO: https://www.baeldung.com/swagger-2-documentation-for-spring-rest-api
	//TODO: http://www.springboottutorial.com/spring-boot-validation-for-rest-services

	//http://localhost:8080/h2-console/
	//http://localhost:8080/swagger-ui.html
	
}
