package com.liquorShop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiquorShopSapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(LiquorShopSapiApplication.class, args);
	}
}
