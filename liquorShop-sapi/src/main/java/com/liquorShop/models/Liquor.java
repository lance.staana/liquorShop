package com.liquorShop.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Liquor {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	private String type;
	
	private String origin;
	
	@Column(name="SELLING_PRICE")
	private Double sellingPrice;
	
	private Double proof;

	public static String LIQUOR_TYPE_BEER = "Beer";
	public static String LIQUOR_TYPE_GIN = "Gin";
	public static String LIQUOR_TYPE_VODKA = "Vodka";
	public static String LIQUOR_TYPE_WHISKEY = "Whiskey";
	public static String LIQUOR_TYPE_TEQUILA = "Tequila";
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public Double getSellingPrice() {
		return sellingPrice;
	}
	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}
	public Double getProof() {
		return proof;
	}
	public void setProof(Double proof) {
		this.proof = proof;
	}
	
}
