INSERT INTO LIQUOR
VALUES(1, 'Absolut Vodka', 'Sweden', 70.00, 750.00, 'Vodka');

INSERT INTO LIQUOR
VALUES(2, 'McAllan', 'Scotland', 70.00, 1400.00, 'Scotch');

INSERT INTO LIQUOR
VALUES(3, 'Don Julio', 'Mexico', 60.00, 1200.00, 'Tequila');

INSERT INTO MEMBER
VALUES(1, 12000.00, 'lance.manryke@uap.asia', 'Lance', 'Manryke');